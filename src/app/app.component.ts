import { Component } from '@angular/core';
import {ToasterConfig} from 'angular2-toaster';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'helmes-sectors-frontend';

  public config: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-bottom-right',
    animation: 'fade',
    showCloseButton: true,
    mouseoverTimerStop: true
  });
}
