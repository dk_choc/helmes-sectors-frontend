import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AddUserSectorComponent } from './components/add-user-sector/add-user-sector.component';
import { ViewUserSectorComponent } from './components/view-user-sector/view-user-sector.component';
import { HomeComponent } from './components/home/home.component';
import { UserSectorInfoComponent } from './components/user-sector-info/user-sector-info.component';

import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    AddUserSectorComponent,
    ViewUserSectorComponent,
    HomeComponent,
    UserSectorInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToasterModule,
    BrowserAnimationsModule,
    LoggerModule.forRoot({
      level: NgxLoggerLevel.TRACE,
      serverLogLevel: NgxLoggerLevel.ERROR
    })
  ],
  providers: [
    ToasterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
