import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUserSectorComponent } from './view-user-sector.component';

describe('ViewUserSectorComponent', () => {
  let component: ViewUserSectorComponent;
  let fixture: ComponentFixture<ViewUserSectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUserSectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUserSectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
