import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SectorService} from '../../services/sector.service';
import {FormBuilder} from '@angular/forms';
import {UserSectorService} from '../../services/user-sector.service';
import {NGXLogger} from 'ngx-logger';
import {ToastService} from '../../services/toast.service';
import {HOME_URL, USER_SECTOR_URL} from '../../util/constants';

@Component({
  selector: 'app-view-user-sector',
  templateUrl: './view-user-sector.component.html',
  styleUrls: ['./view-user-sector.component.css']
})
export class ViewUserSectorComponent implements OnInit {

  public editForm = this.formBuilder.group({
    userSectorInfo: ['', []]
  });
  constructor(private formBuilder: FormBuilder,
              private sectorService: SectorService,
              private userSectorService: UserSectorService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private logger: NGXLogger,
              private toastService: ToastService) {

    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id')) {
        this.getUserSector(params.id);
      }
    });
  }

  ngOnInit() {
  }

  getUserSector(userId: string) {
    this.userSectorService.getUser(userId)
      .subscribe(data => {
        if ( Object.keys(data).length ) {
          this.logger.info('Found user sector', data);
          this.refillEditForm(data);
        } else {
          this.router.navigate([USER_SECTOR_URL]);
          this.toastService.pop('error', 'Error!', 'Cannot find your data, please fill out!');
        }
      });
  }

  refillEditForm(data: any) {
    this.editForm.controls.userSectorInfo.setValue({
      name: data.name,
      sectors: data.sectors,
      termsAgree: true
    });
  }

  onSubmit(userSectorData) {
    this.logger.info('User form submitted', userSectorData);
    const editedUserSectorInfo = {
      id: sessionStorage.getItem('user_id'),
      name: userSectorData.userSectorInfo.name,
      sectors: userSectorData.userSectorInfo.sectors
    };
    this.userSectorService.update( editedUserSectorInfo )
      .subscribe(data => {
        this.logger.info('Updated user sector ', data);
        if ( Object.keys(data).length ) {
          this.logger.info('Updated user sector saved!', data);
          this.router.navigate([HOME_URL]);
          this.toastService.pop('success', 'Success!', 'User sector successfully updated!');
        } else {
          this.logger.warn('Updated sector not saved. Please try again!');
        }
      });
  }

  goBack() {
    this.router.navigate([HOME_URL]);
  }
}
