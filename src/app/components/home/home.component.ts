import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {USER_SECTOR_URL} from '../../util/constants';
import {ToastService} from '../../services/toast.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router,
              private toastService: ToastService) { }

  ngOnInit() {
  }

  addUserSector() {
    this.router.navigate([USER_SECTOR_URL]);
  }

  viewUserSector() {
    const userId = sessionStorage.getItem('user_id');
    if (userId) {
      this.router.navigate([USER_SECTOR_URL, userId]);
    } else {
      this.toastService.pop('error', 'Error!', 'Cannot find your data, please add!');
    }
  }
}
