import {Component, forwardRef, OnInit} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormBuilder, NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
  Validators
} from '@angular/forms';
import {SectorService} from '../../services/sector.service';

@Component({
  selector: 'app-user-sector-info',
  templateUrl: './user-sector-info.component.html',
  styleUrls: ['./user-sector-info.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UserSectorInfoComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => UserSectorInfoComponent),
      multi: true
    }
  ]
})
export class UserSectorInfoComponent implements OnInit, ControlValueAccessor, Validator {

  sectors: any = null;
  public userSectorInfoForm = this.formBuilder.group({
    name: ['', [Validators.required]],
    sectors: ['', [Validators.required, Validators.maxLength(5)]],
    termsAgree: [false, [Validators.pattern('true')]]
  });
  constructor(private formBuilder: FormBuilder,
              private sectorService: SectorService) { }

  ngOnInit() {
    this.sectorService.getSectors()
      .subscribe(data => {
        this.sectors = data;
      });
  }

  get form() { return this.userSectorInfoForm.controls; }

  public onTouched: () => void = () => {};

  writeValue(val: any): void {
    val && this.userSectorInfoForm.setValue(val, { emitEvent: false });
  }
  registerOnChange(fn: any): void {
    this.userSectorInfoForm.valueChanges.subscribe(fn);
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    isDisabled ? this.userSectorInfoForm.disable() : this.userSectorInfoForm.enable();
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this.userSectorInfoForm.valid ? null : { invalidForm: {valid: false, message: 'userSectorInfoForm fields are invalid'}};
  }

}
