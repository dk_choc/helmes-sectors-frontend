import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSectorInfoComponent } from './user-sector-info.component';

describe('UserSectorInfoComponent', () => {
  let component: UserSectorInfoComponent;
  let fixture: ComponentFixture<UserSectorInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSectorInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSectorInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
