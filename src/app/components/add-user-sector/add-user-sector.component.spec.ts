import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserSectorComponent } from './add-user-sector.component';

describe('AddUserSectorComponent', () => {
  let component: AddUserSectorComponent;
  let fixture: ComponentFixture<AddUserSectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUserSectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserSectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
