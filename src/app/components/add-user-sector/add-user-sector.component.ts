import { Component, OnInit } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {SectorService} from '../../services/sector.service';
import {Router} from '@angular/router';
import {UserSectorService} from '../../services/user-sector.service';
import { NGXLogger } from 'ngx-logger';
import {ToastService} from '../../services/toast.service';
import {HOME_URL} from '../../util/constants';

@Component({
  selector: 'app-add-user-sector',
  templateUrl: './add-user-sector.component.html',
  styleUrls: ['./add-user-sector.component.css']
})
export class AddUserSectorComponent implements OnInit {

  public createForm = this.formBuilder.group({
    userSectorInfo: ['', []]
  });
  constructor(private formBuilder: FormBuilder,
              private sectorService: SectorService,
              private userSectorService: UserSectorService,
              private router: Router,
              private logger: NGXLogger,
              private toastService: ToastService) { }

  ngOnInit() {
  }

  onSubmit(userSectorData) {
    this.logger.info('User form submitted', userSectorData);
    const newUserSectorInfo = {
      name: userSectorData.userSectorInfo.name,
      sectors: userSectorData.userSectorInfo.sectors
    };
    this.userSectorService.save( newUserSectorInfo )
      .subscribe(data => {
        this.logger.info('New user sector ', data);
        if ( Object.keys(data).length ) {
          this.logger.info('New user sector saved!', data);
          sessionStorage.setItem('user_id', data.id);
          this.router.navigate([HOME_URL]);
          this.toastService.pop('success', 'Success!', 'New user sector successfully saved!');
        } else {
          this.logger.warn('User sector not saved. Please try again!');
        }
      });
  }

  goBack() {
    this.router.navigate([HOME_URL]);
  }
}
