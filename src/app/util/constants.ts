export const SECTOR_URL = '/sector';
export const SECTOR_ALL_URL = '/sector/all';

export const USER_SECTOR_URL = '/user-sector';

export const HOME_URL = '/home';
