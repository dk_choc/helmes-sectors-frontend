
export class Functions {

  public static isEmpty(object: any) {
    return object == null || (typeof object === 'undefined') || (typeof object === 'string' && object.toString().trim().length === 0);
  }
}
