import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {USER_SECTOR_URL} from '../util/constants';

@Injectable({
  providedIn: 'root'
})
export class UserSectorService {

  constructor(private http: HttpClient) { }

  /**
   * Get user by id.
   * @param {string} id
   * @returns {Observable<Object>}
   */
  public getUser(id: string) {
    const endpoint = USER_SECTOR_URL + '/' + id;
    return this.http.get(this.getUrl(endpoint))
      .pipe(
        map(data => this.extractData(data))
      );
  }

  /**
   * Save new user.
   * @param user
   * @returns {Observable<Object>}
   */
  public save(user) {
    const endpoint = USER_SECTOR_URL;
    const body = JSON.stringify(user);
    const options = {
      headers: this.getHeaders()
    };
    return this.http.post(this.getUrl(endpoint), body, options)
      .pipe(
        map(data => this.extractData(data))
      );
  }

  /**
   * Update existing user.
   * @param user
   * @returns {Observable<Object>}
   */
  public update(user) {
    const endpoint = USER_SECTOR_URL;
    const body = JSON.stringify(user);
    const options = {
      headers: this.getHeaders()
    };
    return this.http.put(this.getUrl(endpoint), body, options)
      .pipe(
        map(data => this.extractData(data))
      );
  }

  /**
   * Makes url from baseUrl and endpoint.
   * @param {string} endpoint.
   * @returns {string} Url.
   */
  private getUrl(endpoint: string): string {
    return environment.baseUrl + endpoint;
  }

  /**
   * Prepares headers for HTTP requests.
   * @returns {HttpHeaders} HTTP request headers.
   * @private
   */
  private getHeaders(): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    return headers;
  }

  /**
   * Extract JSON data from the response.
   * @param response Server response object.
   * @returns {any} Response JSON or an empty JSON object if the response is empty.
   */
  private extractData(response: any): any {
    let data: any;

    try {
      data = response;
    } catch (e) {
      data = {};
    }

    return data || {};
  }
}
