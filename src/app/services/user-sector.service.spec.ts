import { TestBed } from '@angular/core/testing';

import { UserSectorService } from './user-sector.service';

describe('UserSectorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserSectorService = TestBed.get(UserSectorService);
    expect(service).toBeTruthy();
  });
});
