import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment';
import {SECTOR_ALL_URL} from '../util/constants';

@Injectable({
  providedIn: 'root'
})
export class SectorService {

  constructor(
    private http: HttpClient) { }

  /**
   * Get all sectors.
   * @returns {Observable<Object>}
   */
  public getSectors() {
    const url = environment.baseUrl + SECTOR_ALL_URL;
    return this.http.get(url);
  }
}
