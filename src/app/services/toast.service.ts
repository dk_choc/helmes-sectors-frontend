import { Injectable } from '@angular/core';
import {Toast, ToasterService} from 'angular2-toaster';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toasterService: ToasterService) { }

  public pop(toastType: string, toastTitle: string, message: string) {
    const toast: Toast = {
      type: toastType,
      title: toastTitle,
      body: message,
      showCloseButton: true
    };

    this.toasterService.pop(toast);
  }
}
