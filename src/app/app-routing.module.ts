import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {AddUserSectorComponent} from './components/add-user-sector/add-user-sector.component';
import {ViewUserSectorComponent} from './components/view-user-sector/view-user-sector.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'user-sector', component: AddUserSectorComponent},
  {path: 'user-sector/:id', component: ViewUserSectorComponent},
  {path: '**', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
